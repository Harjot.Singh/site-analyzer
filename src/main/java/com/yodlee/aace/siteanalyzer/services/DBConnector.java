package com.yodlee.aace.siteanalyzer.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import com.yodlee.aace.siteanalyzer.model.DBQueryResponse;

@Repository
@Transactional
public class DBConnector {
	@Autowired
	 JdbcTemplate jdbcTemplate;
	 public List<DBQueryResponse> ListAll(String siteid) {
	    String sql = "select sum_info_id, tag.tag, tag.tag_id from tag, sum_info where tag.tag_id=sum_info.tag_id and site_id="+siteid+" and is_ready=1";
	     
	    List<DBQueryResponse> list = jdbcTemplate.query(sql,
	                BeanPropertyRowMapper.newInstance(DBQueryResponse.class));
	     
	    return list;
	}
}
