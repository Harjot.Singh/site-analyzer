package com.yodlee.aace.siteanalyzer.services;

import org.springframework.beans.factory.annotation.Autowired;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;
import com.yodlee.aace.siteanalyzer.model.AttributeAnalysis;
import com.yodlee.aace.siteanalyzer.model.AttributeDetailsItem;
import com.yodlee.aace.siteanalyzer.model.Comparison;
import com.yodlee.aace.siteanalyzer.model.APIResponse;
import com.yodlee.aace.siteanalyzer.model.ItemError;
import com.yodlee.aace.siteanalyzer.model.ItemErrorAnalysis;
import com.yodlee.aace.siteanalyzer.model.MemSiteAccExtItem;
import com.yodlee.aace.siteanalyzer.model.DBQueryResponse;
import com.yodlee.aace.siteanalyzer.model.SiteErrorAnalysis;
import com.yodlee.aace.siteanalyzer.model.SiteRefreshAnalysis;
import com.yodlee.aace.siteanalyzer.splunk.RunQuery;
import org.json.*;
@Service
public class LocalService {
	
	@Autowired
	DBConnector connector;
	
	String attrdesc[] = {"FULL_ACCT_NUMBER","BANK_TRANSFER_CODE", "HOLDER_NAME", "HOLDER_DETAILS" ,"ACCOUNT_DETAILS", "HOLDINGS", "STATEMENTS", "TRANSACTIONS", "STATEMENTS", "TAX", "EBILLS", "PAYMENT_PROFILE", "BASIC_ACCOUNT_INFO", "INTEREST_DETAILS", "PAYMENT_DETAILS", "COVERAGE"};
	String datasetdesc[]= {"BASIC_AGG_DATA", "ACCT_PROFILE", "DOCUMENT", "ADVANCE_AGG_DATA"};
private RunQuery splunkQuerRun = new RunQuery();
	public String getSiteTrend(String siteid, String days, String errorCode, String username,String password) {
		// TODO Auto-generated method stub
		System.out.println("testing");
		String queryStringFailure = "search index=\"itemerrors\" SITE_ID="+siteid+" ERROR_TYPE=\"SITE_ERROR\"\r\n" + 
				"|eval DAY = strftime(strptime(CREATED,\"%Y-%m-%d %H\"),\"%Y-%m-%d\")\r\n" + 
				"|eval HOUR = strftime(strptime(CREATED,\"%Y-%m-%d %H\"),\"%H\")\r\n" + 
				"| table MEM_SITE_ACC_ID CACHE_ITEM_ID TYPE_OF_ERROR COBRAND_ID GATHERER_IP DAY HOUR SUM_INFO_ID SITE_ID REFRESH_SOURCE SERVER_TYPE EXCEPTION_STACK_TRACE SERVER_LATENCY SCRIPT_LATENCY";
		
		
		String queryStringNonSiteError = "search index=\"itemerrors\" SITE_ID=" +siteid+" NOT(ERROR_TYPE=\"SITE_ERROR\") NOT(CACHE_ITEM_ID = 'null' OR CACHE_ITEM_ID=\"-1\")\r\n" + 
				"| dedup SITE_TXN_ID\r\n" + 
				"|eval DAY = strftime(strptime(CREATED,\"%Y-%m-%d %H\"),\"%Y-%m-%d\")\r\n" + 
				"|eval HOUR = strftime(strptime(CREATED,\"%Y-%m-%d %H\"),\"%H\")\r\n" + 
				"| table MEM_SITE_ACC_ID CACHE_ITEM_ID TYPE_OF_ERROR COBRAND_ID GATHERER_IP DAY HOUR SUM_INFO_ID SITE_ID REFRESH_SOURCE SERVER_TYPE EXCEPTION_STACK_TRACE SERVER_LATENCY SCRIPT_LATENCY";
		try {
			String response = splunkQuerRun.process(queryStringFailure, "-"+days+"d", "now", username, password);
			response = response.substring(response.indexOf("\"results\":")+"\"results\":".length(),response.indexOf(", \"highlighted\":{}}"));
			Gson gson = new Gson();
		    Type type = new TypeToken<List<ItemError>>(){}.getType();
		    
		    List<ItemError> contactList = gson.fromJson(response, type);
		  
		    HashMap<String, Integer> ipAnalysisFailure = new HashMap<String, Integer>();
		    HashMap<String, Integer> dateAnalysisFailure = new HashMap<String, Integer>();
		    HashMap<String, Integer> hourAnalysisFailure = new HashMap<String, Integer>();
		    HashMap<String, Integer> serverStatsAnalysisFailure = new HashMap<String, Integer>();
		    HashMap<String, Integer> cobrandAnalysisFailure = new HashMap<String, Integer>();
		   
		    
		    for (ItemError itemError : contactList) {
		    	String err=itemError.getTYPE_OF_ERROR();
		    	if(!err.equals(errorCode))continue;
		    	
		    	int count =0;
		    	if(null!=ipAnalysisFailure.get(itemError.getGATHERER_IP())) {
		    		count = ipAnalysisFailure.get(itemError.getGATHERER_IP());
		    		count++;
		    	}else {
		    		count++;
		    	}
		    	ipAnalysisFailure.put(itemError.getGATHERER_IP(), count);
		    	
		    	int dateCount =0;
		    	if(null!=dateAnalysisFailure.get(itemError.getDAY())) {
		    		dateCount = dateAnalysisFailure.get(itemError.getDAY());
		    		dateCount++;
		    	}else {
		    		dateCount++;
		    	}
		    	dateAnalysisFailure.put(itemError.getDAY(), dateCount);
		    	
		    	int hourCount =0;
		    	if(null!=hourAnalysisFailure.get(itemError.getHOUR())) {
		    		hourCount = hourAnalysisFailure.get(itemError.getHOUR());
		    		hourCount++;
		    	}else {
		    		hourCount++;
		    	}
		    	hourAnalysisFailure.put(itemError.getHOUR(), hourCount);
		    	
		    	int ss =0;
		    	if(null!=serverStatsAnalysisFailure.get(itemError.getSERVER_TYPE())) {
		    		ss = serverStatsAnalysisFailure.get(itemError.getSERVER_TYPE());
		    		ss++;
		    	}else {
		    		ss++;
		    	}
		    	serverStatsAnalysisFailure.put(itemError.getSERVER_TYPE(), ss);
		    	
		    	
		    	int cob =0;
		    	if(null!=cobrandAnalysisFailure.get(itemError.getCOBRAND_ID())) {
		    		cob = cobrandAnalysisFailure.get(itemError.getCOBRAND_ID());
		    		cob++;
		    	}else {
		    		cob++;
		    	}
		    	cobrandAnalysisFailure.put(itemError.getCOBRAND_ID(), cob);
			}
		    
		    
		    
		    response = splunkQuerRun.process(queryStringNonSiteError, "-"+days+"d", "now", username, password);
			response = response.substring(response.indexOf("\"results\":")+"\"results\":".length(),response.indexOf(", \"highlighted\":{}}"));
			gson = new Gson();
		    type = new TypeToken<List<ItemError>>(){}.getType();
		    contactList = gson.fromJson(response, type);
		    
		    
		    HashMap<String, Integer> ipAnalysisSuccess = new HashMap<String, Integer>();
		    HashMap<String, Integer> dateAnalysisSuccess = new HashMap<String, Integer>();
		    HashMap<String, Integer> hourAnalysisSuccess = new HashMap<String, Integer>();
		    HashMap<String, Integer> serverStatsAnalysisSuccess = new HashMap<String, Integer>();
		    HashMap<String, Integer> cobrandAnalysisSuccess = new HashMap<String, Integer>();
		    
		    for (ItemError itemError : contactList) {
		    	String err=itemError.getTYPE_OF_ERROR();
		    	if(!err.equals(errorCode))continue;
		    	
		    	int count =0;
		    	if(null!=ipAnalysisSuccess.get(itemError.getGATHERER_IP())) {
		    		count = ipAnalysisSuccess.get(itemError.getGATHERER_IP());
		    		count++;
		    	}else {
		    		count++;
		    	}
		    	ipAnalysisSuccess.put(itemError.getGATHERER_IP(), count);
		    	
		    	int dateCount =0;
		    	if(null!=dateAnalysisSuccess.get(itemError.getDAY())) {
		    		dateCount = dateAnalysisSuccess.get(itemError.getDAY());
		    		dateCount++;
		    	}else {
		    		dateCount++;
		    	}
		    	dateAnalysisSuccess.put(itemError.getDAY(), dateCount);
		    	
		    	int hourCount =0;
		    	if(null!=hourAnalysisSuccess.get(itemError.getHOUR())) {
		    		hourCount = hourAnalysisSuccess.get(itemError.getHOUR());
		    		hourCount++;
		    	}else {
		    		hourCount++;
		    	}
		    	hourAnalysisSuccess.put(itemError.getHOUR(), hourCount);
		    	
		    	int ss =0;
		    	if(null!=serverStatsAnalysisSuccess.get(itemError.getSERVER_TYPE())) {
		    		ss = serverStatsAnalysisSuccess.get(itemError.getSERVER_TYPE());
		    		ss++;
		    	}else {
		    		ss++;
		    	}
		    	serverStatsAnalysisSuccess.put(itemError.getSERVER_TYPE(), ss);
		    	
		    	
		    	int cob =0;
		    	if(null!=cobrandAnalysisSuccess.get(itemError.getCOBRAND_ID())) {
		    		cob = cobrandAnalysisSuccess.get(itemError.getCOBRAND_ID());
		    		cob++;
		    	}else {
		    		cob++;
		    	}
		    	cobrandAnalysisSuccess.put(itemError.getCOBRAND_ID(), cob);
			}
		    
		    List<Comparison> compareList = new ArrayList<Comparison>();
		    Comparison cpmpare = new Comparison();
		    List<ItemErrorAnalysis> l1List = new ArrayList<ItemErrorAnalysis>();
		    ItemErrorAnalysis level1 = new ItemErrorAnalysis();
		    level1.setCategory("Failure");
		    level1.setValues(ipAnalysisFailure);
		    l1List.add(level1);
		    level1 = new ItemErrorAnalysis();
		    level1.setCategory("Success");
		    level1.setValues(ipAnalysisSuccess);
		    l1List.add(level1);
		    cpmpare.setType("IP Analysis");
		    cpmpare.setMetrics(l1List);
		    compareList.add(cpmpare);
		    
		    
		    cpmpare = new Comparison();
		    l1List = new ArrayList<ItemErrorAnalysis>();
		    level1 = new ItemErrorAnalysis();
		    level1.setCategory("Failure");
		    level1.setValues(dateAnalysisFailure);
		    l1List.add(level1);
		    level1 = new ItemErrorAnalysis();
		    level1.setCategory("Success");
		    level1.setValues(dateAnalysisSuccess);
		    l1List.add(level1);
		    cpmpare.setType("Date Analysis");
		    cpmpare.setMetrics(l1List);
		    compareList.add(cpmpare);
		    
		    
		    cpmpare = new Comparison();
		    l1List = new ArrayList<ItemErrorAnalysis>();
		    level1 = new ItemErrorAnalysis();
		    level1.setCategory("Failure");
		    level1.setValues(hourAnalysisFailure);
		    l1List.add(level1);
		    level1 = new ItemErrorAnalysis();
		    level1.setCategory("Success");
		    level1.setValues(hourAnalysisSuccess);
		    l1List.add(level1);
		    cpmpare.setType("Hour Analysis");
		    cpmpare.setMetrics(l1List);
		    compareList.add(cpmpare);
		    
		    
		    cpmpare = new Comparison();
		    l1List = new ArrayList<ItemErrorAnalysis>();
		    level1 = new ItemErrorAnalysis();
		    level1.setCategory("Failure");
		    level1.setValues(serverStatsAnalysisFailure);
		    l1List.add(level1);
		    level1 = new ItemErrorAnalysis();
		    level1.setCategory("Success");
		    level1.setValues(serverStatsAnalysisSuccess);
		    l1List.add(level1);
		    cpmpare.setType("ServerType Analysis");
		    cpmpare.setMetrics(l1List);
		    compareList.add(cpmpare);
		    
		    
		    cpmpare = new Comparison();
		    l1List = new ArrayList<ItemErrorAnalysis>();
		    level1 = new ItemErrorAnalysis();
		    level1.setCategory("Failure");
		    level1.setValues(cobrandAnalysisFailure);
		    l1List.add(level1);
		    level1 = new ItemErrorAnalysis();
		    level1.setCategory("Success");
		    level1.setValues(cobrandAnalysisSuccess);
		    l1List.add(level1);
		    cpmpare.setType("Cobrand Analysis");
		    cpmpare.setMetrics(l1List);
		    compareList.add(cpmpare);
		    
		    
		    SiteErrorAnalysis siteObject  = new SiteErrorAnalysis();
		    siteObject.setCategory(siteid);
		    siteObject.setCompare(compareList);
		    
		    List<SiteErrorAnalysis> siteList = new ArrayList<SiteErrorAnalysis>();
		    siteList.add(siteObject);
		    
		    APIResponse<SiteErrorAnalysis> res = new APIResponse<>();
		    res.setResult(siteList);
		    res.setSite_id(siteid);
		    
		    
		    List<DBQueryResponse> sqlres  = connector.ListAll(siteid);
		    List<String> containers = new ArrayList<String>();
		    List<String> suminfoids = new ArrayList<>();
		    
		    for(DBQueryResponse r: sqlres)
		    {
		    	containers.add(r.getTAG());
		    	suminfoids.add(r.getSUM_INFO_ID());
		    }
		    
		    res.setContainers(containers);
		    res.setSum_info_ids(suminfoids);
		    
		    
		    JsonElement element = gson.toJsonTree(res, APIResponse.class);
		    
		    return ""+element;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return e.getMessage();
		}
		
		
	}
	
	
	public String getSiteRefreshStats(String siteid,String days ,String username ,String password)
	{
		
		String queryrefreshstats  = "search index=\"refresh_stats\" SITE_ID="+siteid;
		try {
			String response = splunkQuerRun.process(queryrefreshstats, "-"+days+"d", "now", username, password);
			
			response = response.substring(response.indexOf("\"results\":")+"\"results\":".length(),response.indexOf(", \"highlighted\":{}}"));
			
		    JSONArray resultArray=new JSONArray(response);
		    Gson gson=new Gson();
		    Type type = new TypeToken<List<MemSiteAccExtItem>>(){}.getType();
		    
		    
		    HashMap<String,Integer> attribReq=new HashMap<>();
		    HashMap<String,Integer> attribRet=new HashMap<>();
		    for(int i=1;i<=16;i++) {attribReq.put(Integer.toString(i), 0);attribRet.put(Integer.toString(i), 0);}
		    
		    
		    
		    for(int i=0;i<resultArray.length();i++)
		    {
		    	String rawString = resultArray.getJSONObject(i).getString("_raw");
		    	String msaExtJson = rawString.substring(rawString.indexOf("MEM_SITE_ACC_EXT_JSON")+"MEM_SITE_ACC_EXT_JSON=\"".length(), rawString.lastIndexOf("\""));
		        List<MemSiteAccExtItem> list = gson.fromJson(msaExtJson, type);
		        for(MemSiteAccExtItem obj : list)
		        {
		        	List<AttributeDetailsItem> attrs = obj.getAttribDetails();
		        	
		        	for(AttributeDetailsItem attr: attrs)
		        	{
		        		int mc = attribReq.get(attr.getAttributeId());
		        		attribReq.put(attr.getAttributeId(), mc+1);
		        		if(attr.getErrorCode().equals("0"))
		        		{
		        			mc = attribRet.get(attr.getAttributeId());
			        		attribRet.put(attr.getAttributeId(), mc+1);
		        		}
		        	}
		        		
		        	
		        }
		    	
		    }
		    
		    
		    List<AttributeAnalysis> attrs = new ArrayList<>();
		    for(int i=0;i<16;i++)
		    {
		    	AttributeAnalysis obj =new AttributeAnalysis();
		    	obj.setAttribId(Integer.toString(i+1));
		    	obj.setRequestedCount(attribReq.get(Integer.toString(i+1)));
		    	obj.setReturnedCount(attribRet.get(Integer.toString(i+1)));
		    	obj.setAttribName(attrdesc[i]);
		    	attrs.add(obj);
		    }
		    
		    List<SiteRefreshAnalysis> result = new ArrayList<>();

		    SiteRefreshAnalysis obj1=new SiteRefreshAnalysis();
		    obj1.setDataSetId("1");
		    obj1.setDataSetName(datasetdesc[0]);

		    List<AttributeAnalysis> l1 = new ArrayList<>();
		    l1.add(attrs.get(4));
		    l1.add(attrs.get(5));
		    l1.add(attrs.get(6));
		    l1.add(attrs.get(7));
		    l1.add(attrs.get(12));
		    
		    obj1.setAttribAnalysis(l1);
		    result.add(obj1);
		    
		    
		    SiteRefreshAnalysis obj2=new SiteRefreshAnalysis();
		    obj2.setDataSetId("2");
		    obj2.setDataSetName(datasetdesc[1]);

		    List<AttributeAnalysis> l2 = new ArrayList<>();
		    l2.add(attrs.get(0));
		    l2.add(attrs.get(1));
		    l2.add(attrs.get(2));
		    l2.add(attrs.get(3));
		    l2.add(attrs.get(11));
		    
		    obj2.setAttribAnalysis(l2);
		    result.add(obj2);
		    
		    SiteRefreshAnalysis obj3=new SiteRefreshAnalysis();
		    obj3.setDataSetId("3");
		    obj3.setDataSetName(datasetdesc[2]);

		    List<AttributeAnalysis> l3 = new ArrayList<>();
		    l3.add(attrs.get(8));
		    l3.add(attrs.get(9));
		    l3.add(attrs.get(10));
		    
		    obj3.setAttribAnalysis(l3);
		    result.add(obj3);
		    
		    
		    
		    
		    SiteRefreshAnalysis obj4=new SiteRefreshAnalysis();
		    obj4.setDataSetId("4");
		    obj4.setDataSetName(datasetdesc[3]);

		    List<AttributeAnalysis> l4 = new ArrayList<>();
		    l4.add(attrs.get(13));
		    l4.add(attrs.get(14));
		    l4.add(attrs.get(15));
		    
		    
		    obj4.setAttribAnalysis(l4);
		    result.add(obj4);
		    	

			
		    APIResponse<SiteRefreshAnalysis> res = new APIResponse<>();
		    res.setResult(result);
		    res.setSite_id(siteid);
		    
		    
		    List<DBQueryResponse> sqlres  = connector.ListAll(siteid);
		    List<String> containers = new ArrayList<String>();
		    List<String> suminfoids = new ArrayList<>();
		    
		    for(DBQueryResponse r: sqlres)
		    {
		    	containers.add(r.getTAG());
		    	suminfoids.add(r.getSUM_INFO_ID());
		    }
		    
		    res.setContainers(containers);
		    res.setSum_info_ids(suminfoids);
		    
		    
		    JsonElement element = gson.toJsonTree(res, APIResponse.class);
		    
		    return ""+element;
		}
		 catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return e.getMessage();
			}
		
	}
}

