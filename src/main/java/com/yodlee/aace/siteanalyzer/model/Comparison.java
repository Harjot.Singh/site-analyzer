package com.yodlee.aace.siteanalyzer.model;

import java.util.List;

public class Comparison {

	private String type;
	private List<ItemErrorAnalysis> metrics;
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public List<ItemErrorAnalysis> getMetrics() {
		return metrics;
	}
	public void setMetrics(List<ItemErrorAnalysis> metrics) {
		this.metrics = metrics;
	}
	@Override
	public String toString() {
		return "Comparison [type=" + type + ", metrics=" + metrics + ", getType()=" + getType() + ", getMetrics()="
				+ getMetrics() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

	
	
}
