package com.yodlee.aace.siteanalyzer.model;

import java.util.List;

public class SiteRefreshAnalysis {
	private String dataSetId;
	private String dataSetName;
	private List<AttributeAnalysis> attribAnalysis;
	public String getDataSetId() {
		return dataSetId;
	}
	public void setDataSetId(String dataSetId) {
		this.dataSetId = dataSetId;
	}
	public List<AttributeAnalysis> getAttribAnalysis() {
		return attribAnalysis;
	}
	public void setAttribAnalysis(List<AttributeAnalysis> attribAnalysis) {
		this.attribAnalysis = attribAnalysis;
	}
	public String getDataSetName() {
		return dataSetName;
	}
	public void setDataSetName(String dataSetName) {
		this.dataSetName = dataSetName;
	}
	
}
