package com.yodlee.aace.siteanalyzer.model;

import java.util.HashMap;

public class ItemErrorAnalysis {

	public String category;
	public HashMap<String, Integer> values;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public HashMap<String, Integer> getValues() {
		return values;
	}
	public void setValues(HashMap<String, Integer> values) {
		this.values = values;
	}
	@Override
	public String toString() {
		return "ItemErrorAnalysis [category=" + category + ", values=" + values + ", getCategory()=" + getCategory()
				+ ", getValues()=" + getValues() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
	
}
