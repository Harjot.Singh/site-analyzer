package com.yodlee.aace.siteanalyzer.model;

public class AttributeDetailsItem {
	private String attributeId;
	private String refreshStatus;
	private String errorCode;
	public String getAttributeId() {
		return attributeId;
	}
	public void setAttributeId(String attributeId) {
		this.attributeId = attributeId;
	}
	public String getRefreshStatus() {
		return refreshStatus;
	}
	public void setRefreshStatus(String refreshStatus) {
		this.refreshStatus = refreshStatus;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	
	@Override
	public String toString()
	{
		return "attributeId="+attributeId+", refreshStatus="+refreshStatus+", errorCode="+errorCode;
		
	}
}
