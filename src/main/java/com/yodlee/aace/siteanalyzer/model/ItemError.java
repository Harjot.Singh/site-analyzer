package com.yodlee.aace.siteanalyzer.model;

public class ItemError {

	 private String MEM_SITE_ACC_ID;
	 private String CACHE_ITEM_ID;
	 private String TYPE_OF_ERROR;
	 private String COBRAND_ID;
	 private String GATHERER_IP;
	 private String DAY;
	 private String HOUR;
	 private String SUM_INFO_ID;
	 private String SITE_ID;
	 private String REFRESH_SOURCE;
	 private String SERVER_TYPE;
	 private String EXCEPTION_STACK_TRACE;
	 private String SERVER_LATENCY;
	 private String SCRIPT_LATENCY;
	 private String RESPONSE_TYPE;
	 private String SITE_TXN_ID;
	 private String TRANSACTION_ID;
	 
	public String getDAY() {
		return DAY;
	}
	public void setDAY(String dAY) {
		DAY = dAY;
	}
	public String getHOUR() {
		return HOUR;
	}
	public void setHOUR(String hOUR) {
		HOUR = hOUR;
	}
	public String getMEM_SITE_ACC_ID() {
		return MEM_SITE_ACC_ID;
	}
	public void setMEM_SITE_ACC_ID(String mEM_SITE_ACC_ID) {
		MEM_SITE_ACC_ID = mEM_SITE_ACC_ID;
	}
	public String getCACHE_ITEM_ID() {
		return CACHE_ITEM_ID;
	}
	public void setCACHE_ITEM_ID(String cACHE_ITEM_ID) {
		CACHE_ITEM_ID = cACHE_ITEM_ID;
	}
	public String getTYPE_OF_ERROR() {
		return TYPE_OF_ERROR;
	}
	public void setTYPE_OF_ERROR(String tYPE_OF_ERROR) {
		TYPE_OF_ERROR = tYPE_OF_ERROR;
	}
	public String getCOBRAND_ID() {
		return COBRAND_ID;
	}
	public void setCOBRAND_ID(String cOBRAND_ID) {
		COBRAND_ID = cOBRAND_ID;
	}
	public String getGATHERER_IP() {
		return GATHERER_IP;
	}
	public void setGATHERER_IP(String gATHERER_IP) {
		GATHERER_IP = gATHERER_IP;
	}
	public String getSUM_INFO_ID() {
		return SUM_INFO_ID;
	}
	public void setSUM_INFO_ID(String sUM_INFO_ID) {
		SUM_INFO_ID = sUM_INFO_ID;
	}
	public String getSITE_ID() {
		return SITE_ID;
	}
	public void setSITE_ID(String sITE_ID) {
		SITE_ID = sITE_ID;
	}
	public String getREFRESH_SOURCE() {
		return REFRESH_SOURCE;
	}
	public void setREFRESH_SOURCE(String rEFRESH_SOURCE) {
		REFRESH_SOURCE = rEFRESH_SOURCE;
	}
	public String getSERVER_TYPE() {
		return SERVER_TYPE;
	}
	public void setSERVER_TYPE(String sERVER_TYPE) {
		SERVER_TYPE = sERVER_TYPE;
	}
	public String getEXCEPTION_STACK_TRACE() {
		return EXCEPTION_STACK_TRACE;
	}
	public void setEXCEPTION_STACK_TRACE(String eXCEPTION_STACK_TRACE) {
		EXCEPTION_STACK_TRACE = eXCEPTION_STACK_TRACE;
	}
	public String getSERVER_LATENCY() {
		return SERVER_LATENCY;
	}
	public void setSERVER_LATENCY(String sERVER_LATENCY) {
		SERVER_LATENCY = sERVER_LATENCY;
	}
	public String getSCRIPT_LATENCY() {
		return SCRIPT_LATENCY;
	}
	public void setSCRIPT_LATENCY(String sCRIPT_LATENCY) {
		SCRIPT_LATENCY = sCRIPT_LATENCY;
	}
	public String getRESPONSE_TYPE() {
		return RESPONSE_TYPE;
	}
	public void setRESPONSE_TYPE(String rESPONSE_TYPE) {
		RESPONSE_TYPE = rESPONSE_TYPE;
	}
	public String getSITE_TXN_ID() {
		return SITE_TXN_ID;
	}
	public void setSITE_TXN_ID(String sITE_TXN_ID) {
		SITE_TXN_ID = sITE_TXN_ID;
	}
	public String getTRANSACTION_ID() {
		return TRANSACTION_ID;
	}
	public void setTRANSACTION_ID(String tRANSACTION_ID) {
		TRANSACTION_ID = tRANSACTION_ID;
	}
}
