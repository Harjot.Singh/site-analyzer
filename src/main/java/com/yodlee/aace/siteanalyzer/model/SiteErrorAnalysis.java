package com.yodlee.aace.siteanalyzer.model;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class SiteErrorAnalysis {
	private String category;
	private List<Comparison> compare;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public List<Comparison> getCompare() {
		return compare;
	}
	public void setCompare(List<Comparison> compare) {
		this.compare = compare;
	}
	@Override
	public String toString() {
		return "SiteErrorAnalysis [category=" + category + ", compare=" + compare + ", getCategory()=" + getCategory()
				+ ", getCompare()=" + getCompare() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
	
	
}
