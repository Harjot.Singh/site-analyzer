package com.yodlee.aace.siteanalyzer.model;

public class AttributeAnalysis {
	private String attribId;
	private String attribName;
	private int requestedCount;
	private int returnedCount;
	public String getAttribId() {
		return attribId;
	}
	public void setAttribId(String attribId) {
		this.attribId = attribId;
	}
	public String getAttribName() {
		return attribName;
	}
	public void setAttribName(String attribName) {
		this.attribName = attribName;
	}
	public int getRequestedCount() {
		return requestedCount;
	}
	public void setRequestedCount(int requestedCount) {
		this.requestedCount = requestedCount;
	}
	public int getReturnedCount() {
		return returnedCount;
	}
	public void setReturnedCount(int returnedCount) {
		this.returnedCount = returnedCount;
	}
	
	
	
}
