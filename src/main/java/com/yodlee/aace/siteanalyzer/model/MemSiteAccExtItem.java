package com.yodlee.aace.siteanalyzer.model;

import java.util.List;

public class MemSiteAccExtItem {
	private String datasetId;
	private String refreshStatus;
	private String errorCode;
	private List<AttributeDetailsItem> attribDetails;
	public String getDatasetId() {
		return datasetId;
	}
	public void setDatasetId(String datasetId) {
		this.datasetId = datasetId;
	}
	public String getRefreshStatus() {
		return refreshStatus;
	}
	public void setRefreshStatus(String refreshStatus) {
		this.refreshStatus = refreshStatus;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public List<AttributeDetailsItem> getAttribDetails() {
		return attribDetails;
	}
	public void setAttribDetails(List<AttributeDetailsItem> attribDetails) {
		this.attribDetails = attribDetails;
	}
	
	@Override
	public String toString()
	{
		String attr="[";
		for(int i=0;i<attribDetails.size();i++)attr+="{"+attribDetails.get(i).toString()+"},";
		attr+="]";
		return "datasetId="+datasetId+", refreshStatus="+refreshStatus+", errorCode="+errorCode+", attribDetails="+attr;
		
		
	}
}
