package com.yodlee.aace.siteanalyzer.model;

public class DBQueryResponse {
	private String SUM_INFO_ID, TAG, TAG_ID;
	
	DBQueryResponse()
	{
		SUM_INFO_ID=TAG=TAG_ID="";
	}
	
	

	public String getSUM_INFO_ID() {
		return SUM_INFO_ID;
	}

	public void setSUM_INFO_ID(String mSUM_INFO_ID) {
		SUM_INFO_ID = mSUM_INFO_ID;
	}

	public String getTAG() {
		return TAG;
	}

	public void setTAG(String mTAG) {
		TAG = mTAG;
	}

	public String getTAG_ID() {
		return TAG_ID;
	}

	public void setTAG_ID(String mTAG_ID) {
		TAG_ID = mTAG_ID;
	}
	
}
