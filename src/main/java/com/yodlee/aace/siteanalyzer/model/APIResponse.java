package com.yodlee.aace.siteanalyzer.model;

import java.util.List;

public class APIResponse<T> {
	private String site_id;
	private List<String> sum_info_ids;
	private List<String> containers;
	private List<T> result;
	public String getSite_id() {
		return site_id;
	}
	public void setSite_id(String site_id) {
		this.site_id = site_id;
	}
	public List<String> getSum_info_ids() {
		return sum_info_ids;
	}
	public void setSum_info_ids(List<String> sum_info_ids) {
		this.sum_info_ids = sum_info_ids;
	}
	public List<String> getContainers() {
		return containers;
	}
	public void setContainers(List<String> containers) {
		this.containers = containers;
	}
	
	public List<T> getResult(){return result;}
	public void setResult(List<T> res) {result=res;}
}
