package com.yodlee.aace.siteanalyzer.splunk;
/*
 * Copyright (c) 2019 Yodlee, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of Yodlee, Inc.
 * Use is subject to license terms.
 */


/**
 * @author Pravesh Karn
 * @since Date: Nov 20, 2019
 */
public interface IAuthorizer {
    String decrypt(String text);
    String encrypt(String text);
}
