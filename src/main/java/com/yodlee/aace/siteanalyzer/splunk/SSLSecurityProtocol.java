package com.yodlee.aace.siteanalyzer.splunk;


public enum SSLSecurityProtocol {

    TLSV1_2 {

        @Override
        public String toString() {

            return "TLSv1.2";

        }

    },

    TLSV1_1 {

        @Override
        public String toString() {

            return "TLSv1.1";

        }

    },

    TLSV1 {

        @Override
        public String toString() {

            return "TLSv1";

        }

    },

    SSLV3 {

        @Override
        public String toString() {

            return "SSLv3";

        }

    }
}

