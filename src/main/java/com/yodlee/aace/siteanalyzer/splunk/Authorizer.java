/*
 * Copyright (c) 2019 Yodlee, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of Yodlee, Inc.
 * Use is subject to license terms.
 */

package com.yodlee.aace.siteanalyzer.splunk;


import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import static com.yodlee.aace.siteanalyzer.splunk.SplunkConstants.ENCRYPT_DECRYPT_KEY;



/**
 * @author Pravesh Karn
 * @since Date: Nov 20, 2019
 */

/**
 * Encrypts and Decrypts texts
 */
@Service
public class Authorizer implements IAuthorizer {

    private static final org.slf4j.Logger logger= LoggerFactory.getLogger(Authorizer.class);

    /**
     * Decrypts the text
     * @param text
     * @return Decrypted String
     */
    public String decrypt(String text) {

        logger.info("Executing Authorizer.decrypt() with params text:{}",text);
        logger.info("Returning decrypted text from Authorizer.decrypt()");
        return EncryptDecryptTool.decrypt(text, ENCRYPT_DECRYPT_KEY);
    }

    /**
     * Encrypts the text
     * @param text
     * @return Encrypted String
     */
    public String encrypt(String text) {

        logger.info("Executing Authorizer.encrypt() with params:{}",text);
        logger.info("Returning encrypted text from Authorizer.encrypt()");
        return EncryptDecryptTool.encrypt(text, ENCRYPT_DECRYPT_KEY);
    }
}
