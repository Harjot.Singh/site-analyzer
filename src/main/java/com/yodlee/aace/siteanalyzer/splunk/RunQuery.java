package com.yodlee.aace.siteanalyzer.splunk;


import java.util.HashMap;
import java.util.Map;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.yodlee.aace.siteanalyzer.SiteAnalyzerApplication;
public class RunQuery {
	//
//  @Inject
//  private SplunkGetUserRest splunkGetUserRest;

  private static final Logger logger = LoggerFactory.getLogger(RunQuery.class);
  public static final String REST_URL_END = "/results?output_mode=json&count=0";

  public static final String REST_URL= "https://splunkapi.yodlee.com/";
  public static final String REST_URL_MID = "/services/search/jobs/";
  public static final String AUTHORISATION = "Authorization";
  public static final String SPLUNK_CONSTANT = "Splunk ";
  public static final String RESULTS = "results";

  public String process(String queryString,String startTime,String endTime,String username,String password) throws Exception {

      logger.info("Inside Fire Splunk Query");
      
      //String queryString = "search index=\"gatherer_logs\" USER.ITEM.SITE_ID=643 USER.ITEM.CODE IN (402) |table USER.SI.SITEACCOUNT_ID SITE_ID";
     // String startTime = "-10h";
     //String endTime = "now";

      //Splunk login
      SplunkGetUserRest splunkGetUserRest = new SplunkGetUserRest();
     String splunkSession =  splunkGetUserRest.login(username,password);

      //Get session ID
      String sid = splunkGetUserRest.getSid(false, queryString, startTime,endTime,splunkSession);
      String url = REST_URL + REST_URL_MID + sid + REST_URL_END;

      Map<String, String> params = new HashMap<>();
      params.put(AUTHORISATION, SPLUNK_CONSTANT + null);

      logger.info("Calling SplunkGetUserRest.get() from fire query .process()");

      String response = splunkGetUserRest.get(url, params,splunkSession);
      return response;
  }
}
