package com.yodlee.aace.siteanalyzer.splunk;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.net.ssl.*;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * @Author Pravesh Karn
 * @Date 12/18/2019
 **/

@Component
public class SplunkGetUserRest {

  //  @Inject
    public OkHttpRequestCalls okHttpRequestCalls = new OkHttpRequestCalls();

    @Autowired
    private Authorizer authorization;

 

    LocalDateTime lastRefreshed = null;


    private String splunkSession = null;

    static SSLSocketFactory delegate;
    public static final String REST_URL= "https://splunkapi.yodlee.com/";


    static SSLSecurityProtocol sslSecurityProtocol1;

    private static final String SPLUNK_CONSTANT = "Splunk ";

    private static final String AUTHORISATION = "Authorization";
    private static final String REST_URL_MID = "/services/search/jobs/";
    private static final String REST_URL_END = "/results?output_mode=json";
    private static final String RESULTS = "results";
    private static final String DATABASE = "dataBase";

    private static final Logger logger = LoggerFactory.getLogger(SplunkGetUserRest.class);


    protected static SSLSecurityProtocol sslSecurityProtocol = SSLSecurityProtocol.TLSV1_2;

    private static SSLSocketFactory sslSocketFactory;

    static {
        try {
            sslSocketFactory = createSSLFactory();
        } catch (NoSuchAlgorithmException e) {
            logger.error("Caught No such Algorithm exception : {}", e.getMessage());
        }
    }


    public static SSLSocketFactory createSSLFactory() throws NoSuchAlgorithmException {


        TrustManager[] trustAll = new TrustManager[]{new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }

            public void checkClientTrusted(X509Certificate[] certs, String authType) throws CertificateException {
//
            }

            public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
//
            }

        }};

        try {

            SSLContext context;

            switch (sslSecurityProtocol) {

                case TLSV1_2:

                case TLSV1_1:

                case TLSV1:
                    context = SSLContext.getInstance("TLS");
                    break;

                default:
                    context = SSLContext.getInstance("SSL");
            }

            context.init(null, trustAll, new java.security.SecureRandom());

            return new SplunkHttpsSocketFactory(context.getSocketFactory(), sslSecurityProtocol);

        } catch (Exception e) {
            logger.error("Error setting up SSL socket factory");
            throw new NoSuchAlgorithmException("Error setting up SSL socket factory: " + e, e);
        }
    }


    /**
     * Function to verify hostname
     */


    private static final HostnameVerifier HOSTNAME_VERIFIER = (s, sslSession) -> true;


    public static final Map<String, String> dbMapping = new HashMap<>();

    static {
        dbMapping.put("dbcana011", "dbcana01s");
        dbMapping.put("scexag071", "scexag07s");
        dbMapping.put("exayod1", "exayods");
        dbMapping.put("auspr04", "auspr04s");
        dbMapping.put("auspr011", "auspr01s");
        dbMapping.put("inpra011", "inprda01s");
        dbMapping.put("auspr031", "auspr03s");
        dbMapping.put("scexpr011", "scexpr01s");
        dbMapping.put("yoddemo1", "yoddemos");
    }


    public static final class SplunkHttpsSocketFactory extends SSLSocketFactory {


        private final SSLSocketFactory delegate;

        private final SSLSecurityProtocol sslSecurityProtocol;


        private SplunkHttpsSocketFactory(SSLSocketFactory delegate, SSLSecurityProtocol securityProtocol) {

            this.delegate = delegate;
            this.sslSecurityProtocol = securityProtocol;
        }

        private Socket configure(Socket socket) {

            if (socket instanceof SSLSocket) {
                ((SSLSocket) socket).setEnabledProtocols(new String[]{sslSecurityProtocol.toString()});
            }

            return socket;
        }

        @Override
        public String[] getDefaultCipherSuites() {
            return delegate.getDefaultCipherSuites();
        }

        @Override
        public String[] getSupportedCipherSuites() {
            return delegate.getSupportedCipherSuites();
        }

        @Override
        public Socket createSocket(Socket socket, String s, int i, boolean b) throws IOException {
            return configure(delegate.createSocket(socket, s, i, b));
        }

        @Override
        public Socket createSocket() throws IOException {
            return configure(delegate.createSocket());
        }

        @Override
        public Socket createSocket(String s, int i) throws IOException {
            return configure(delegate.createSocket(s, i));
        }

        @Override
        public Socket createSocket(String s, int i, InetAddress inetAddress, int i1)
                throws IOException {
            return configure(delegate.createSocket(s, i, inetAddress, i1));
        }

        @Override
        public Socket createSocket(InetAddress inetAddress, int i) throws IOException {
            return configure(delegate.createSocket(inetAddress, i));
        }

        @Override
        public Socket createSocket(InetAddress inetAddress, int i, InetAddress inetAddress1, int i1)
                throws IOException {
            return configure(delegate.createSocket(inetAddress, i, inetAddress1, i1));
        }
    }


    /**
     * Function to login to splunk service
     */

    public void login() {

        logger.info("Entering GatewayMFADumpProcessor.login() without any param");


        String url = REST_URL + "services/auth/login";

        Map<String, String> params = new LinkedHashMap<>();


        logger.info("Calling Authorization.decrypt() from GatewayMFADumpProcessor.login()");
       


        params.put("username", "XXXX");
        params.put("password", "XXXXX");

        logger.info("Calling post() from GatewayMFADumpProcessor.login()");
        String response = post(url, params);

        splunkSession = response.substring(response.indexOf("<sessionKey>") + 12, response.lastIndexOf("</sessionKey>"))
                .trim();


        logger.info("Exiting GatewayMFADumpProcessor.login()");
    }
    public String login(String username,String password) {

        logger.info("Entering GatewayMFADumpProcessor.login() with params");


        String url = REST_URL + "services/auth/login";

        Map<String, String> params = new LinkedHashMap<>();


        logger.info("Calling Authorization.decrypt() from GatewayMFADumpProcessor.login()");
       


        params.put("username", username);
        params.put("password", password);

        logger.info("Calling post() from GatewayMFADumpProcessor.login()");
        String response = post(url, params);

        splunkSession = response.substring(response.indexOf("<sessionKey>") + 12, response.lastIndexOf("</sessionKey>"))
                .trim();


        logger.info("Splunk session created");
        return splunkSession;
    }

    /**
     * Post the http request to splunk service for particular url
     *
     * @param url
     * @param params
     * @return
     */
    public String post(String url, Map<String, String> params) {

        logger.info("Entering GatewayMFADumpProcessor.post() with params url:{} and params:{}", url, params);

        HttpsURLConnection.setDefaultHostnameVerifier(HOSTNAME_VERIFIER);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);

        String response = "";

        try {

            response = okHttpRequestCalls.okHttpPostParams(url, params, SPLUNK_CONSTANT + splunkSession);
        }
        catch (IOException e){
            logger.error("Caught exception in SplunkGetUserRest.post() {}", e.getMessage());
        }

        logger.info("Returning response body from MFATypeFromDumpProcessor.post()");
        return response;
    }
    
    public String post(String url, Map<String, String> params,String splunkSession) {

        logger.info("Inside the post to validate the session", url, params);

        HttpsURLConnection.setDefaultHostnameVerifier(HOSTNAME_VERIFIER);
        HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);

        String response = "";

        try {

            response = okHttpRequestCalls.okHttpPostParams(url, params, SPLUNK_CONSTANT + splunkSession);
        }
        catch (IOException e){
            logger.error("Caught exception in SplunkGetUserRest.post() {}", e.getMessage());
        }

        logger.info("Returning response body from MFATypeFromDumpProcessor.post()");
        return response;
    }


    public String getSid(boolean reload, String queryString, String earliestTime,String endTime) {

        logger.info("Entering GatewayMFADumpProcessor.getSid() with params reload:{},querrString:{} and earliest_time:{}", reload, queryString, earliestTime);

        if (splunkSession == null) {
            login();
        }

        String sid = null;
        try {
            Map<String, String> params = new LinkedHashMap<>();

            String url = null;
            if (queryString != null) {

                url = REST_URL + "services/search/jobs?output_mode=json";
                params.put("earliest_time", earliestTime);
                params.put("latest_time", endTime);
                params.put("search", queryString);
            }

            logger.info("Calling GatewayMFADumpProcessor.post() from GatewayMFADumpProcessor.getsid()");
            String response = post(url, params);

            if (queryString != null) {
                sid = response.substring(response.indexOf(':') + 2, response.lastIndexOf('\"'));
            } else {
                sid = response.substring(response.indexOf("<sid>") + 5, response.lastIndexOf("</sid>")).trim();
            }

            logger.info("Calling GatewayMFADumpProcessor.getJobstatus() from GatewayMFADumpProcessor.getSid()");
            getJobstatus(sid, 0);


        } catch (Exception e) {

            logger.info("Caught Exception with message :{}", e.getMessage());
            if (e.getMessage().contains("401")) {
                clearSession();
                login();

                if (!reload) {
                    logger.info("Calling GatewayMFADumpProcessor.getSid()");
                    sid = getSid(true, queryString, earliestTime,endTime);
                }
            }
        }


        logger.info("Returning sid:{} from GatewayMFADumpProcessor.getSid()", sid);
        return sid;
    }
    public String getSid(boolean reload, String queryString, String earliestTime,String endTime,String splunkSession) {

        logger.info("Entering GatewayMFADumpProcessor.getSid() with params reload:{},querrString:{} and earliest_time:{}", reload, queryString, earliestTime);

        if (splunkSession == null) {
            return "loginFailure";
        }

        String sid = null;
        try {
            Map<String, String> params = new LinkedHashMap<>();

            String url = null;
            if (queryString != null) {

                url = REST_URL + "services/search/jobs?output_mode=json";
                params.put("earliest_time", earliestTime);
                params.put("latest_time", endTime);
                params.put("search", queryString);
            }

            logger.info("Calling GatewayMFADumpProcessor.post() from GatewayMFADumpProcessor.getsid()");
            String response = post(url, params,splunkSession);

            if (queryString != null) {
                sid = response.substring(response.indexOf(':') + 2, response.lastIndexOf('\"'));
            } else {
                sid = response.substring(response.indexOf("<sid>") + 5, response.lastIndexOf("</sid>")).trim();
            }

            logger.info("Getting job Status....");
            getJobstatus(sid, 0,splunkSession);


        } catch (Exception e) {

            logger.info("Caught Exception with message :{}", e.getMessage());
            if (e.getMessage().contains("401")) {
                clearSession();
                login();

                if (!reload) {
                    logger.info("Calling GatewayMFADumpProcessor.getSid()");
                    sid = getSid(true, queryString, earliestTime,endTime,splunkSession);
                }
            }
        }


        logger.info("Returning sid:{} from GatewayMFADumpProcessor.getSid()", sid);
        return sid;
    }


    /**
     * Gets the Job status
     *
     * @param sid
     * @param count
     * @return
     * @throws Exception
     */
    public String getJobstatus(String sid, int count) {

        for(int count1=count ; count1<301; ++count1) {
            logger.info("Gettiing status: ", sid, count1);

            String response = get(REST_URL + "services/search/jobs/" + sid, null);

            String done = response.substring(response.indexOf("<s:key name=\"isDone\">") + 21,
                    response.indexOf("<s:key name=\"isDone\">") + 22);

            String status = "";

            if (done.equals("1")) {
                status = response.substring(response.indexOf("<s:key name=\"isFailed\">") + 23,
                        response.indexOf("<s:key name=\"isFailed\">") + 24);
                return status;
            } else {
                try {
                    Thread.sleep(10000);
                } catch (Exception e) {
                    logger.error("Caught exception", e);
                }

            }
        }
        return null;
    }
    
    public String getJobstatus(String sid, int count,String splunkSession) {

        for(int count1=count ; count1<301; ++count1) {
            logger.info("Gettiing status: ", sid, count1);

            String response = get(REST_URL + "services/search/jobs/" + sid, null,splunkSession);

            String done = response.substring(response.indexOf("<s:key name=\"isDone\">") + 21,
                    response.indexOf("<s:key name=\"isDone\">") + 22);

            String status = "";

            if (done.equals("1")) {
                status = response.substring(response.indexOf("<s:key name=\"isFailed\">") + 23,
                        response.indexOf("<s:key name=\"isFailed\">") + 24);
                return status;
            } else {
                try {
                    Thread.sleep(1000);
                } catch (Exception e) {
                    logger.error("Caught exception", e);
                }

            }
        }
        return null;
    }

    /**
     * Gets the http response for a url
     *
     * @param url
     * @param params
     * @return
     */

    public String get(String url, Map<String, String> params) {
        logger.info("Entering into GatewayMFADumpProcessor.get() with params url:{} and map:{}", url, params);
        try {

            HttpsURLConnection.setDefaultHostnameVerifier(HOSTNAME_VERIFIER);
            HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);

            String response = okHttpRequestCalls.okHttpGet(url, SPLUNK_CONSTANT+splunkSession);

            logger.info("Returning response body from GatewayMFADumpProcessor.get()");

            return response;

        } catch (Exception e) {

            logger.error("Caught Exception:{} with message", e.getMessage());

        }
        return "";
    }
    public String get(String url, Map<String, String> params,String splunkSession) {
        logger.info("Entering into GatewayMFADumpProcessor.get() with params url:{} and map:{}", url, params);
        try {

            HttpsURLConnection.setDefaultHostnameVerifier(HOSTNAME_VERIFIER);
            HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);

            String response = okHttpRequestCalls.okHttpGet(url, SPLUNK_CONSTANT+splunkSession);
            return response;

        } catch (Exception e) {

            logger.error("Caught Exception:{} with message", e.getMessage());

        }
        return "";
    }


    // getting users from Splunk in case Yuva is not giving the response //
    public JSONArray getUsersFromSplunk(String suminfo) throws JSONException {

        logger.info("Executing...getUsersFromSplunk...with param suminfo:{}", suminfo);
        String query2 = "";
        if (suminfo != null && !suminfo.isEmpty()) {
            query2 = "search index=itemerrors sourcetype=item_errors SUM_INFO_ID=" + suminfo + " TYPE_OF_ERROR=0 DBID!=sdbcaf06  CACHE_ITEM_ID!=-1 MEM_SITE_ACC_ID!=-1 |eval cachMSA=CACHE_ITEM_ID.MEM_SITE_ACC_ID | dedup cachMSA | eval temp=CACHE_ITEM_ID.DBID | dedup temp |Head 5 |Table SUM_INFO_ID,MEM_SITE_ACC_ID,CACHE_ITEM_ID,DBID,NUM_SUCCESSFUL_REFRESH,NUM_ITEM_ACCOUNTS,NEW_TRANSACTIONS MATCH |sort 0 -MATCH";
        }

        logger.info("splunkSession at the top  :{}", splunkSession);
        clearSession();

        if (splunkSession == null) {
            login();
        }

        String queryString = query2;

        logger.info(queryString);

        String startTime = "-2d@h";
        String endTime = "now";
        String sid = getSid(false, queryString, startTime,endTime);
        String url = REST_URL + REST_URL_MID + sid + REST_URL_END;

        logger.info("url:{}", url);
        logger.info("splunkSession :{}", splunkSession);

        Map<String, String> params = new HashMap<>();
        params.put(AUTHORISATION, SPLUNK_CONSTANT + splunkSession);

        String response = get(url, params);
        logger.info("....response:{} ", response);

        JSONObject responseObject = new JSONObject(response);
        JSONArray resultArray = responseObject.getJSONArray(RESULTS);

        JSONArray splunkReturnArray = new JSONArray();

        int resultArraylength;
        if (resultArray.length() > 5) {
            resultArraylength = 5;
        } else {
            resultArraylength = resultArray.length();
        }

        for (int i = 0; i < resultArraylength; i++) {

            JSONObject agentObject = (JSONObject) resultArray.get(i);

            JSONObject obj = new JSONObject();

            obj.put("cacheItemId", agentObject.getString("CACHE_ITEM_ID"));
            obj.put("memSiteAccId", agentObject.getString("MEM_SITE_ACC_ID"));

            if (dbMapping.get(agentObject.getString("DBID")) != null) {
                obj.put(DATABASE, dbMapping.get(agentObject.getString("DBID")));
                logger.info("...DB Name Altered : {}", dbMapping.get(agentObject.getString("DBID")));
            } else {
                obj.put(DATABASE, agentObject.getString("DBID"));
            }

            splunkReturnArray.put(obj);

        }

        logger.info("...size :{} ", splunkReturnArray.length());
        logger.info("splunkService:{}", splunkReturnArray);
        clearSession();
        return splunkReturnArray;
    }

    public void clearSession() {
        logger.info("Entering GatewayMFADumpProcessor.clearSession()");

        logger.info("clear the session ");

        splunkSession = null;
        logger.info("Exiting GatewayMFADumpProcessor.clearSession()");
    }

    public String returnSession()
    {
        logger.info("Entering ReturnSession()");
        return splunkSession;
    }

}
