/*
 * Copyright (c) 2019 Yodlee, Inc. All Rights Reserved.
 *
 * This software is the confidential and proprietary information of Yodlee, Inc.
 * Use is subject to license terms.
 */

package com.yodlee.aace.siteanalyzer.splunk;


import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;


/**
 * @author Pravesh Karn
 * @since Date: Nov 20, 2019
 */

/**
 * Encyption and decryption of key
 */

@Component
public class EncryptDecryptTool {
    private EncryptDecryptTool(){}

    private static SecretKeySpec secretKey;
    private static byte[] key;
    private static final org.slf4j.Logger logger= LoggerFactory.getLogger(EncryptDecryptTool.class);

    /**
     * Sets the key to encrypt later
     * @param myKey
     */
    public static void setKey(String myKey){
        logger.info("Executing EncryptDecryptTool.setKey() with params:{}",myKey);
        MessageDigest sha = null;

        try {

            key = myKey.getBytes(StandardCharsets.UTF_8);

            sha = MessageDigest.getInstance("SHA-1");

            key = sha.digest(key);

            key = Arrays.copyOf(key, 16);

            secretKey = new SecretKeySpec(key, "AES");

        }

        catch (NoSuchAlgorithmException e) {


            logger.error("Caught NoSuchAlgorithmException in EncryptDecryptTool.setKey() : ",e);
        }
        logger.info("Exiting from EncryptDecryptTool.setKey() ");
    }

    /**
     * Encrypts the key
     * @param strToEncrypt
     * @param secret key
     * @return
     */

    public static String encrypt(String strToEncrypt, String secret)

    {
        logger.info("Executing EncryptDecryptTool.encrypt() with params strToEncrypt:{} and secret:{}",strToEncrypt,secret);
        try

        {

            setKey(secret);

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");

            cipher.init(Cipher.ENCRYPT_MODE, secretKey);

            logger.info("Returning  Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(\"UTF-8\"))) from EncryptDecryptTool.encrypt() ");
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));

        }

        catch (Exception e)

        {
            logger.error("Caught Exception in EncryptDecryptTool.encrypt(): ",e);


        }
        logger.info("Returning  null from EncryptDecryptTool.encrypt() ");
        return null;

    }

    /**
     * Decrypts the key
     * @param strToDecrypt
     * @param secret
     * @return
     */

    public static String decrypt(String strToDecrypt, String secret)

    {
        logger.info("Executing EncryptDecryptTool.decrypt() with params strToDecrypt:{} and secret:{}",strToDecrypt,secret);
        try

        {

            setKey(secret);

            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");

            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            logger.info("Returning String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt))) from EncryptDecryptTool.decrypt()");
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));

        }

        catch (Exception e)

        {
            logger.error(" Caught exception in EncryptDecryptTool.decrypt(): ",e);


        }
        logger.info(" Returning null from EncryptDecryptTool.decrypt()");
        return null;

    }

}