package com.yodlee.aace.siteanalyzer.splunk;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


import java.io.IOException;
import java.util.Map;


/**
 * @author Pravesh Karn
 * @Date 14 Jan 2020
 * @Purpose : This class is used ofr making different rest calls to an api.
 */
@Component
public class OkHttpRequestCalls {

	//  @Inject
	private OkHttpClient okHttpClient = new OkHttpClient();

	public static final Logger logger= LoggerFactory.getLogger(OkHttpRequestCalls.class);

	public static final String CONTENT_TYPE = "Content-Type";
	public static final String AUTHORIZATON = "Authorization";
	public static final String APPLICATION_JSON="application/json";
	public static final String APPLICATION_JSON_CHARSET="application/json; charset=utf-8";


	/**
	 * this method is used for making post request to an api with String request
	 * @param url
	 * @param req
	 * @param token
	 * @return String response
	 * @throws IOException
	 */
	public String okHttpPost(String url, String req, String token) throws IOException {
		//logger.info("Entering OkHttpRequestCalls.okHttpPost()");
		MediaType json = MediaType.parse(APPLICATION_JSON_CHARSET);
		RequestBody requestBody = RequestBody.create(json, req);
		Request request = null;
		if(token != null) {
			request = new Request.Builder()
					.url(url)
					.post(requestBody)
					.addHeader(CONTENT_TYPE, APPLICATION_JSON)
					.addHeader(AUTHORIZATON, token)
					.build();
		}
		else{
			request = new Request.Builder()
					.url(url)
					.post(requestBody)
					.addHeader(CONTENT_TYPE, APPLICATION_JSON)
					.build();
		}
		Response responseOkHttp = okHttpClient.newCall(request).execute();
		String response = responseOkHttp.body().string();
		//logger.info("Exiting from OkHttpRequestCalls.okHttpPost()");

		return response;
	}

	public String okHttpPostQC(String url, String req){

		//logger.info("Entering OkHttpRequestCalls.okHttpPostQC()");
		try {
			MediaType json = MediaType.parse(APPLICATION_JSON_CHARSET);
			RequestBody requestBody = RequestBody.create(json, req);

			Request request = new Request.Builder()
					.url(url)
					.post(requestBody)
					.addHeader(CONTENT_TYPE, APPLICATION_JSON)
					.build();

			Response responseOkHttp = okHttpClient.newCall(request).execute();
			String response = responseOkHttp.body().string();
			return response;
		}catch (IOException e){
			//logger.info("caught IOException {}",e);
		}
		return null;
	}

	public String okHttpPostQC2(String url, String req, String token) {
		try {
			MediaType json = MediaType.parse(APPLICATION_JSON_CHARSET);
			RequestBody requestBody = RequestBody.create(json, req);
			Request request = new Request.Builder()
					.url(url)
					.post(requestBody)
					.addHeader(CONTENT_TYPE, APPLICATION_JSON)
					.addHeader(AUTHORIZATON, "Bearer " + token)
					.build();

			Response responseOkHttp = okHttpClient.newCall(request).execute();
			String response = responseOkHttp.body().string();
			return response;
		}catch (IOException e){
			//logger.info("caught IOException {}",e);
		}
		return null;
	}


	/**
	 * this method is used for making a post request with request as a Map.
	 * @param url
	 * @param params
	 * @param token
	 * @return String response
	 * @throws IOException
	 */
	public String okHttpPostParams(String url, Map<String, String> params, String token) throws IOException {
		FormBody.Builder builder = new FormBody.Builder();
		for(Map.Entry<String,String> entry : params.entrySet()){
			builder.add(entry.getKey(), entry.getValue());
		}
		RequestBody requestBody = builder.build();
		Request request = new Request.Builder()
				.url(url)
				.post(requestBody)
				.addHeader(CONTENT_TYPE, "application/x-www-form-urlencoded")
				.addHeader(AUTHORIZATON, token)
				.build();
		Response responseOkHttp = okHttpClient.newCall(request).execute();
		String response = responseOkHttp.body().string();
		return response;
	}


	/**
	 * This method is used for making a Get request
	 * @param url
	 * @return String response
	 * @throws IOException
	 */
	public String okHttpGet(String url) throws IOException {
		Request request = new Request.Builder()
				.url(url)
				.get()
				.build();
		Response responseOkHttp = okHttpClient.newCall(request).execute();
		String response = responseOkHttp.body().string();
		return response;
	}


	/**
	 * This method is used for making a GET request with token
	 * @param url
	 * @param token
	 * @return String response
	 * @throws IOException
	 */
	public String okHttpGet(String url, String token) throws IOException {
		//logger.info("Entering OkHttpRequestCalls.okHttpGet()");
		Request request = new Request.Builder()
				.url(url)
				.get()
				.addHeader(CONTENT_TYPE, "application/x-www-form-urlencoded")
				.addHeader(AUTHORIZATON, token)
				.build();
		Response responseOkHttp = okHttpClient.newCall(request).execute();
		String response = responseOkHttp.body().string();
		//logger.info("Exiting from OkHttpRequestCalls.okHttpGet()");
		return response;
	}


	/**
	 * This method is used for making PUT request
	 * @param url
	 * @param req
	 * @param token
	 * @return String response
	 * @throws IOException
	 */
	public String okHttpPut(String url, String req, String token) throws IOException {
		//logger.info("Entering okHttpRequestCalls.okHttpPut() ");
		MediaType json = MediaType.parse(APPLICATION_JSON_CHARSET);
		RequestBody requestBody = RequestBody.create(json, req);
		Request request = null;
		if(token != null) {
			request = new Request.Builder()
					.url(url)
					.put(requestBody)
					.addHeader(CONTENT_TYPE, APPLICATION_JSON)
					.addHeader(AUTHORIZATON, token)
					.build();
		}
		else{
			request = new Request.Builder()
					.url(url)
					.put(requestBody)
					.addHeader(CONTENT_TYPE, APPLICATION_JSON)
					.build();
		}
		Response responseOkHttp = okHttpClient.newCall(request).execute();
		String response = responseOkHttp.body().string();
		//logger.info("Exiting from okHttpRequestCalls.okHttpPut()");
		return response;
	}
}

