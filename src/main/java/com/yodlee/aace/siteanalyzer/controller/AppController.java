package com.yodlee.aace.siteanalyzer.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yodlee.aace.siteanalyzer.services.LocalService;


@Controller

public class AppController {

	@Autowired
	private LocalService service;
	
	
	
	@ResponseBody
	@RequestMapping(value ="/getsitetrend", method= RequestMethod.GET ,produces = "application/json")
	public synchronized String getsitetrend(HttpServletRequest req)
	{
			try {
				
				String siteid = req.getParameter("siteid");
				String days = req.getParameter("days");
				String user=req.getParameter("user");
				String pass=req.getParameter("pass");
				String errorCode=req.getParameter("error");
				
				return service.getSiteTrend(siteid,days,errorCode, user,pass);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
				return "Incorrect Parameter";
			}
		}
	
	@ResponseBody
	@RequestMapping(value ="/getsiterefreshstats", method= RequestMethod.GET ,produces = "application/json")
	public synchronized String getsiterefreshstats(HttpServletRequest req)
	{
			try {
				String siteid = req.getParameter("siteid");
				String user=req.getParameter("user");
				String pass=req.getParameter("pass");
				String days = req.getParameter("days");
				return service.getSiteRefreshStats(siteid,days,user,pass);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
				return "Incorrect Parameter";
			}
		}
	

}
